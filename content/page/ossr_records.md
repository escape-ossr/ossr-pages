---
title: OSSR Records
subtitle: List of records in the OSSR or under curation
comments: false
menu:
  main:
    parent: infrastructure
    weight: 3
---

For a more detailed view, browse the [OSSR directly on Zenodo](https://zenodo.org/communities/escape2020).

