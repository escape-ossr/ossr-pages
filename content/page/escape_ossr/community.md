---
title: Community building
subtitle: Bringing together experts of the field
comments: false
---

The OSSR is part of the ESCAPE open collaboration. With regular collaboration meetings of the OSSR and a core team of developers and reviewers we continuously enhance the OSSR and help building towards a common infrastructure.

If you are onboarding your project, you are invited to the community exchange during the OSSR collaboration meetings. Present there your project and benefit from the experience of colleagues from the field!