---
title: Certification
subtitle: OSSR is building towards a trustworthy repository
comments: false
---

The OSSR [onboarding](/page/contribute/onboarding) requires you to prove basic quality of your software, which also means that you can receive an OSSR badge. In the ESCAPE collaboration we work on enhancing the quality review process and make the review even more transparent and trustwothy. Stay tuned!