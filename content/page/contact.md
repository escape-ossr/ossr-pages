---
title: Contact us
subtitle: 
comments: false
---

# Contact us

## General questions
You can reach us via email to [contact us directly](mailto:kay.graf@fau.de)

## Technical issues
OSSR technical developments are discussed on [the eossr gitlab issues](https://gitlab.com/escape-ossr/eossr/-/issues).    

If opening an issue is not an option for you, you may [contact us directly](mailto:vuillaume@lapp.in2p3.fr).

## Stay informed

You can sign up for our yet to be established news list [already now](https://forms.gle/JsgQAM6Qr2ZfNTzK8).
