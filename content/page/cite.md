---
title: Cite the OSSR
subtitle: 
comments: false
---

# Cite the OSSR

To cite the OSSR as a project, please use

_Vuillaume T, Al-Turany M, Füßling M et al. The ESCAPE Open-source Software and Service Repository. Open Research Europe 2023, 3:46 (https://doi.org/10.12688/openreseurope.15692.2)_



Note that if you want to cite specific records in the OSSR, you should use the specific DOI of that record as provided by Zenodo.

