---
title: OSSR events
subtitle: Collaboration meetings, workshops and training events
comments: false
---

# Workshops

## WOSSL 2020

![WOSSL 2020](https://indico.in2p3.fr/event/21698/logo-1594925232.png)

Workshop on Open-source Software Lifecycles: https://indico.in2p3.fr/event/21698/

# Training events

## ESCAPE OSSR School 2021

![ESCAPE OSSR School 2021](https://indico.in2p3.fr/event/20306/logo-1725324874.png)

School page: https://escape2020.github.io/school2021/     
Training material: https://zenodo.org/records/5093909

## ESCAPE OSSR School 2022

![ESCAPE OSSR School 2022](https://indico.in2p3.fr/event/26913/logo-2092833116.png)

School page: https://escape2020.github.io/school2022/     
Training material: https://zenodo.org/records/6981096