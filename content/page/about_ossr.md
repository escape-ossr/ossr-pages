---
title: About the OSSR
subtitle: The collaboration behind the Open Software and Service Repository
comments: false
---

The Open Software and Service Repository is one building stone emerging from the ESCAPE project in EOSC (European Open Science Cloud), and further supported by the ESCAPE open collaboration.

The group members [meet regularly](https://indico.in2p3.fr/category/844/) and hold OSSR collaboration meetings three times per year.

The OSSR group develops the ORRS, curates the repository, performs a review on all entries and strengthens the community by offering a fruitful environment for exchange.

Read more about, and cite the OSSR:

_Vuillaume T, Al-Turany M, Füßling M et al. The ESCAPE Open-source Software and Service Repository. Open Research Europe 2023, 3:46 (https://doi.org/10.12688/openreseurope.15692.2)_