---
title: CodeMeta Generator
subtitle: Online OSSR CodeMeta generator
comments: false
menu:
  main:
    parent: tools
    weight: 2
---

The [online OSSR CodeMeta generator](https://escape2020.pages.in2p3.fr/wp3/codemeta-generator/) allows you to generate
a `codemeta.json` file for your project.
The file can then be added to your software root directory.
