---
title: CodeMeta to Zenodo 
subtitle: CodeMeta to Zenodo metadata converter
comments: false
menu:
  main:
    parent: tools
    weight: 2
---

# CodeMeta to Zenodo

A CodeMeta to Zenodo converter has been implemented in [the eOSSR library](https://escape2020.pages.in2p3.fr/wp3/eossr/).
It can be used as a command line tool or as a Python library (please refer to the eOSSR documentation).

An online version is also [availalble here](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.in2p3.fr%2Fescape2020%2Fwp3%2Feossr/HEAD?urlpath=voila%2Frender%2Fdocs%2Fmetadata%2Fvalidate_codemeta.ipynb).


# The metadata jungle

A view of the metadata jungle and existing converters early 2023 (note this might not be exhaustive).


[![](https://lucid.app/publicSegments/view/31f02de0-e92d-4839-b4df-8cbdcc3970a6/image.png)](https://lucid.app/lucidchart/f1b4a51a-8766-4bc1-915d-bf562c51123b/edit?invitationId=inv_32ce79ce-1a84-4848-8a74-4e724d9aff19&page=0_0#)

