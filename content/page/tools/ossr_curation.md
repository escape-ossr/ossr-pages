---
title: OSSR Curation
subtitle: Curation platform for OSSR records
comments: false
menu:
  main:
    parent: tools
    weight: 2
---

The [OSSR curation platform](https://gitlab.com/escape-ossr/ossr-curation) allows us to review and curate OSSR records openly and transparently 
(which is not possible at the moment on Zenodo).
It uses the [eOSSR](https://gitlab.com/escape-ossr/eossr) library to interact with the OSSR and triggers
GitLab merge requests when a request is submitted to the OSSR on Zenodo.