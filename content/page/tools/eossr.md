---
title: eOSSR
subtitle: ESCAPE OSSR library
comments: false
menu:
  main:
    parent: tools
    weight: 2
---

The `eOSSR` is the OSSR Python package.
It can be used to interact with the OSSR, find records based on text or metadata searches, upload or download records
It also contains the OSSR metadata implementation and the core tools to handle them (convert, validate, etc.).

- Source code: https://gitlab.com/escape-ossr/eossr
- Documentation: https://escape-ossr.gitlab.io/eossr/
