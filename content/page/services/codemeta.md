---
title: CodeMeta schema
subtitle: Metadata for software within the OSSR
comments: false
menu:
  main:
    parent: infrastructure
    weight: 2
---

The [CodeMeta project](https://codemeta.github.io/) defines a standard metadata syntax (schema) specifically design
to describe "_scientific software and code_". We use this schema to describe all entries to the OSSR. Ensuring the correct use of the schema is therefore crucial for the findability of the software in the OSSR context.

Besides allowing that the GitLab-Zenodo OSSR environment works, **including a metadata file to your project will make it FAIR compliant**. Information from the `codemeta.json` file will also automatically be migrated to the zenodo metadata, so keeping this information coherent is important in the context of the OSSR.

![Software Metadata cluster](/img/20221011_Software_Metadata.png)


## How to generate a `codemeta.json` file ?

You can either generate the file manually or use our [CodeMeta generator](https://escape2020.pages.in2p3.fr/wp3/codemeta-generator/).

To find more information about the OSSR metadata schema and its technical implementation,
please refer to [the eOSSR documentation](https://escape-ossr.gitlab.io/eossr/metadata.html)

We **strongly suggest** to provide a metadata file as complete as possible, every metadata entry is important! A more complete and detailed metadata file will facilitate and encourage the FAIR principles.

## How to check the validity of the file

In order to check the validity of the file, you can use our [validation tools](/page/tools/codemeta_validator).
