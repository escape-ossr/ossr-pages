---
title: How to publish to Zenodo 
subtitle: From your repository or through web interface 
comments: false
menu:
  main:
    parent: contribute
    weight: 3
---

# How to publish Software

You can integrate your software directly from GitHub or GitLab, which allows you to keep your record in synch with your new releases automatically. If you upload by hand, note that you are required to follow the OSSR metadata standards as described in the [codemeta description](/page/services/codemeta).
In all three cases, a `codemeta.json` file has to be added to your report/zenodo record, and you have to add your project to the ESCAPE2020 community.

## 1. Create the codemeta.json file
Include a `codemeta.json` metadata file into the root directory of your project. Learn [here how](/page/services/codemeta/).

## 2. Upload your project to Zenodo
 
###  From GitLab
You will need to link your GitLab project with Zenodo and enable the CI/CD to synch your project. 
Learn how to easily do it in few clicks in [the eOSSR documentation](https://escape2020.pages.in2p3.fr/wp3/eossr/).

###  From GitHub
- Check how to trigger the [GitHub-Zenodo automatically integration](https://guides.github.com/activities/citable-code/).
- Ensure the correspondence of metadata between the `codemeta.json` and Zenodo metadata once the publication to Zenodo is done.
To help you do so, you may use the [eOSSR library](https://escape2020.pages.in2p3.fr/wp3/eossr/) and its codemeta2zenodo converter.
 
###  Manually using Zenodo web interface

This method is discouraged as you will not benefit from the automatisation implemented in the above methods but could be prefered in very specific use cases.

1. Go to the [_new upload_ page on Zenodo.org](https://zenodo.org/deposit/new)
2. Fill the metadata in the web interface.
3. Add all files you want to include in the upload, including the `codemeta.json` directly at the root of the projet. Note that in this case, you must ensure the agreement between the metadata in the codemeta file and the metadata entered manually in Zenodo!
4. Save and publish.

## 3. Add your project to the ESCAPE 2020 community
Edit your project on Zenodo. Under "Communities" search for ["ESCAPE OSSR"](https://zenodo.org/communities/escape2020) and add it. Save the record. This will trigger the curation process to the OSSR.

![escape2020 community in Zenodo](/img/zenodo_escape_community.png)

----------------

You can also check this general quick descriptive *maze* on how to make an upload to the repository;
[*ESCAPE the maze*](https://zenodo.org/record/3743489#.XylVGxMzZTY). 
