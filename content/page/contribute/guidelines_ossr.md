---
title: Guidelines and Rules of Participation
comments: false
menu:
  main:
    parent: infrastructure
    weight: 1
---

Please follow the steps detailed in the
[Onboarding procedure checklist]({{< relref "onboarding" >}})
to finalise your contribution and the onboarding process.

----------------------------

The following guidelines, agreed within the ESCAPE WP3 and gathered in the ESCAPE deliverable 3.7 - 
"License, provenance and metadata guidelines for the software and service repository" (the link to the document will 
be shared soon), indicate how to provide software to the OSSR and under what conditions, in particular regarding 
licensing and provenance. Integration of a new entry in the OSSR is submitted to a curation phase that will 
validate its compliance to the following guidelines. 

# Guidelines and rules of participation to the ESCAPE OSSR

As the ESCAPE OSSR is built on Zenodo, [its policies](https://about.zenodo.org/policies/) apply.

In addition, we have the following recommendations: 

1. Any digital resource provided into the OSSR **must contain a license**. 
    - It most provide a machine-readable description of the license. 
    - The  application  of  the  FAIR  principles  to  the  products  offered  in  the  OSSR 
    results  in  the  necessity  to  assign  licenses  to  all  relevant  parts,  including 
    software code, containerized and bundled software and metadata. 
 
2.  We  **strongly  recommend** to  provide  an  open-source  license.  The  choice  of  a 
**permissive open-source license is advised**. 
    - The Terms of Use for the OSSR require contributors/providers to ensure that 
    all copyright is respected when offering software products through the OSSR. 
    Thus, compatibility between the licenses of derivative or combined works must 
    be  checked  before  the  on-boarding  into  the  OSSR  and  done  by  the 
    contributor/provider side.  
    - The choices of proprietary licenses must be justified. 
    - If no license is assigned yet, it is recommended to use either the MIT or BSD-3 Clause license. 
    - The metadata file itself is considered to be offered under a [CC0 license](
    https://creativecommons.org/publicdomain/zero/1.0/ ). 
    - For  contributed  software,  [guidelines](https://escape2020.pages.in2p3.fr/wp3/licensing ) 
    are  collected  and  offered  to  the contributors to manage the licenses in their software product.
    - Final  ESCAPE  open  science  products,  as  well  as  container  images,  are 
    expected to follow these guidelines too. 
 
3.  We  **strongly  recommend**  that  any  software  provided  to  the  OSSR  incorporates  a 
**metadata file** that describes the software to be uploaded. The use of the CodeMeta 
project schema, and therefore the addition of a codemeta.json metadata file into the 
root directory of the resource is recommended for the OSSR. 
    - The  metadata  file  can  be  generated  at using the [CodeMeta generator](
    https://codemeta.github.io/codemeta-generator/). 
    - The minimum metadata information to be added into any digital resource is 
    the one required to create a new upload or a new version within Zenodo (see 
    Metadata for the OSSR section). However, we strongly recommend to create a metadata file as complete as possible. 
    - In relation with the previous point, it is recommended to provide a summary of user-visible changes between 
    versions (e.g. provide a 'release note' with every new software release as well as fill this 
    field within the codemeta.json) together with  the  OSSR  software  product  or  its  documentation.  
    This  is  essential provenance information and helps fulfilling the FAIR principles. 
    - The verification that the entry contains the minimum provenance information 
    defined in this document is the responsibility of the software provider. 
    - To  date,  the  only  ZenodoCI  compatible  metadata  context  file  is  the `codemeta.json` schema file. 
     - Providing a `codemeta.json` file with your software, in concurrence with the use 
     of the ZenodoCI will allow the provider to define all software metadata using a 
     single  model  and  implementation.  The  integration  of  the  software  into  the 
     OSSR,  as  well  as  the  use  of  other  services  (such  as  the  production  of  a 
     container for your software) will be based on this metadata. 

# Recommendations for container image creation

Using an unversioned 3rd party image as the base for a container makes the resulting container image dependent on what the upstream 3rd party put in the latest version of their container. Thus, making the container unlikely reproducible in the close future.
 
   E.g; `FROM continuumio/miniconda3`

To make a container image reproducible we **strongly recommend** to follow the following recommendations;

1. The FROM statement should include the full registry URL

   E.g; `FROM docker.io/continuumio/miniconda3`

2. The FROM statement should reference a specific version of the base container

   E.g; `FROM docker.io/continuumio/miniconda3:4.10.3p0`
 
