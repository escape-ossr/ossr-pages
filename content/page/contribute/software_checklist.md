---
title: Prepare your repository
comments: false
menu:
  main:
    parent: contribute
    weight: 2
---
For a detailed list, check out the
[guidelines]({{< relref "guidelines_ossr" >}}).


## Checklist for Software

All points are required, unless they're marked as "optional". Please consider the optional points. They usually provide benefit to the repostory users.


### Software Package Checklist

Your project should meet the following standards:

- [X] It is published as a stable versioned release of the project.
- [X] It is under an open-source license.
- [X] Follow a reasonable set of software development / software engineering practices, see for example [presentations at the WOSSL workshop](https://gitlab.in2p3.fr/escape2020/wp3/wossl/-/wikis/Best-Practices-for-software-development), [Research Software Engineers](https://rse.dlr.de/), or [the Netherlands eScience Center Guide](https://guide.esciencecenter.nl/)
- [X] Consider providing a [container image]({{< relref "/page/services/containerization" >}})
  (optional)

- [X] [Create a`codemeta.json` file]({{< relref "/page/services/codemeta.md" >}}) following the OSSR requirements (you can check your file using our validation tools). It is essential that you provide a `codemeta.json` file in your project on zenodo.

- [X] Provide a pointer to your version control development platform in the codemeta file (GitHub, GitLab...)
