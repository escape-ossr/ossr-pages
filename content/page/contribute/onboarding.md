---
title: Onboarding to the OSSR
subtitle: An Overview and Step-by-step Tutorial
comments: false
menu:
  main:
    parent: contribute
    weight: 2
---

# Process Overview

## Why and What to Onboard?

You can onboard your scientific software, public datasets [limited in size](https://about.zenodo.org/policies/), 
container images, or repositories with full analyses environments to the OSSR.
You may do so by completing our onboarding process described here and validating the [requirements]({{< relref "guidelines_ossr" >}}).

## Who is Involved?

- **Onboarder**: You should be author or maintainer to the software you are onboarding to the OSSR and be available as contact for the process. You will need an account at [Zenodo](https://zenodo.org/signup/).
- **Reviewer**: A reviewer from the OSSR project will check that your contribution meets the requirements and publish it to the OSSR.

## What Needs to be Done?

- **Follow the checklists** provided in the [software check list]({{< relref "software_checklist" >}}). For guidelines, see these pages for the separate steps.
- **Register your contribution** To register your project, add your project to Zenodo and submit your record to the "ESCAPE OSSR" community.

- **Get reviewed and published publication** After a positive check by the reviewer, your contribution will be added to the OSSR. 

# Step-by-step

## 1. Document your software

We want to make sure that the software is easy to use and understand. To this end we ask for additional documentation of your software in the following form:

- the list of documentation is currently under [review](https://gitlab.com/escape-ossr/ossr-pages/-/issues/29) and will be added here shortly.

## 2. Adapting Your Contribution

For a full description of requirements to your software, see the [software check list]({{< relref "software_checklist" >}}).

## 3. Publish to Zenodo

You can upload your contribution to Zenodo either from your repository or manually, see this [tutorial](/page/contribute/publish_overview).

- Make sure your zenodo metadata matches the codemeta data.
- Check out [eossr]({{< relref "/page/tools/eossr" >}}) and how to automatise the upload to the OSSR.
- submit to the "ESCAPE OSSR" community

## 4. Review Process

Your request for accepting your contribution to the OSSR will be checked for compliance to the requirements and then be accepted. You may follow the curation process in the corresponding merge request on our [curation project](https://gitlab.com/escape-ossr/ossr-curation/-/merge_requests). Your reviewer will contact you through Zenodo to address any issues.

* The validity of the `codemeta.json` is automatically checked.
* The compliance with the [software check list]({{< relref "software_checklist" >}}) is confirmed by the reviewer.
* The documentation is evaluated by the reviewer

After confirmation by the reviewer, your contribution will be added to the OSSR and is then also findable from the OSSR interface.
