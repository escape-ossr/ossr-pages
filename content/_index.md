# Welcome to the ESCAPE OSSR!

[Browse the OSSR content](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&type=software).

## What is it?

The ESCAPE Open-source Scientific Software and Service Repository (OSSR) is a sustainable open-access repository to share scientific software, services and datasets to the astro-particle-physics-related communities and enable open science.
It is built as a curated  [Zenodo community](https://zenodo.org/communities/escape2020) integrated with several tools to enable a complete software life-cycle.
The ESCAPE Zenodo community welcomes entries that support the software and service projects in the OSSR
such as user-support documentation, tutorials, presentations and training activities. It also encourages the archival of documents and material that disseminate and support the goals of ESCAPE.


## How to contribute to the ESCAPE OSSR?

You can onboard your project right now - [see here](/page/contribute/onboarding/) how.

Learn more about our projects in this website or [contact us](/page/contact)!
 
## Research infrastructures and Science Projects in the OSSR

Directly find software and dataset related to our scientific partners in the OSSR:

| [![CTA](/img/logo_cta_255x100.png)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=CTA) | [![LSST](/img/logo_lsst_267x100.png)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=LSST) | [![Virgo](/img/logo_egovirgo_150x100.jpg)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=Virgo) | 
|---|---|---|
| [![km3net](/img/logo_km3net_150x100.png)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=km3net) | [![elt](/img/logo_elt_150x100.png)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=ESO) | [![EST](/img/logo_est_150x100.jpg)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=EST) |
| [![SKA](/img/logo_ska_150x100.jpg)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=SKA) | [![JIVE](/img/logo_jive_178x100.jpg)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=JIVE) | [![FAIR](/img/logo_fair_150x100.jpg)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=FAIR) | 
| [![LHC](/img/logo_hl_lhc_198x100.png)](https://zenodo.org/communities/escape2020/search?page=1&size=20&q=&keywords=LHC) | | |

 
## Cite the OSSR

To cite the OSSR as a project, please use http://doi.org/10.17616/R31NJN5V

<object type="image/svg+xml" data="https://www.re3data.org/public/badges/l/light/100013827.svg"><img src="https://www.re3data.org/public/badges/l/light/100013827.png" style="max-width:100%"></object>

 -----------------
 
**Please note** that this page is constantly updated with the latest WP3 development.
