# Outreach material

This folder contains outreach material for the OSSR which can be freely used to advertize the OSSR.

## Logos and Stickers

* Logo_ESCAPE_OSSR_full.png: logo with full OSSR title
* Logo_ESCAPE_OSSR_small.png: logo with abbreviation only
* Sticker_ESCAPE_OSSR.png: Sticker for webpages of OSSR entries

## Introducing OSSR

* Intro_OSSR_slides.odp/.pdf: Slides for a short introduction of OSSR at your team/meeting
* Poster_OSSR.odp/.pdf: A1 poster for use at conferences and meeting locations

## Inviting interested people and communities

* Use the invitation email template to inform people about the OSSR
