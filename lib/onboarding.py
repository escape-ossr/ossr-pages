from redminelib import Redmine
import argparse


def write_markdown_onboarding_list_from_redmine(output,
                                                redmine_url='https://project.escape2020.de/',
                                                project_name='wp3-ossr',
                                                tracker_name='Integration'):

    redmine = Redmine(redmine_url)
    project = redmine.project.get(project_name)

    title = 'ESCAPE OSSR onboarding catalog'

    abstract = 'Here is a list of software and services that are currently under the onboarding process within the OSSR. You may see the details of the process on [the issue tracker](https://project.escape2020.de/projects/wp3-ossr)'

    with open(output, 'w') as file:

        file.write(f'# {title}')
        file.write('\n\n')
        file.write(abstract)
        file.write('\n\n\n')


        for issue in project.issues:
            if tracker_name is not None and issue.tracker.name == tracker_name:

                # Exclude TEMPLATE
                if 'TEMPLATE' in issue.subject:
                    continue

                file.write(f'## {issue}\n\n')

                file.write(f'{issue.description.split("#")[0]}\n\n')

                for item in list(issue.custom_fields):
                    if item.value != '':
                        file.write(f'{item.name}: {item.value}    \n')

                file.write('\n')



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Create the onboarding page from the Redmine issue tracker')
    parser.add_argument('output', help='Path to the output filename in markdown')
    args = parser.parse_args()

    write_markdown_onboarding_list_from_redmine(args.output)
