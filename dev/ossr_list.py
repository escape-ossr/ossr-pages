from pathlib import Path
from datetime import datetime
import gitlab
from ossr_curation.config import GITLAB_URL, project_id, zenodo_token
from eossr.api.ossr import get_ossr_records
from eossr.api.zenodo import ZenodoAPI
from eossr.api.zenodo.http_status import HTTPStatusError
from eossr.api.ossr import escape_community

ZENODO_URL = "https://zenodo.org/records/"


def create_markdown_table(records, pending_requests):
    table = "| Software | Status |\n|----------|-----|\n"
    for record in records:
        title = record.title
        url = ZENODO_URL + str(record.id)
        table += f"| {title} | [![accepted](https://img.shields.io/badge/OSSR-accepted_%E2%9C%94%EF%B8%8F-green)]({url}) |\n"

    for mr in get_ossr_curation_opened_requests(pending_requests):
        title = mr.title.replace("[CURATE]", "").strip()
        url = mr.web_url
        table += f"| {title} | [![onboarding](https://img.shields.io/badge/OSSR-onboarding_%E2%8F%B3-orange)]({url}) |\n"

    return table


def get_conceptrecid(pending_request):
    rec_id = pending_request.data["topic"]["record"]
    try:
        record = pending_request.record
        recid = record.data["conceptrecid"]
    except HTTPStatusError:
        deposit = query_deposit(rec_id, access_token=zenodo_token, sandbox=sandbox).json()
        recid = deposit["conceptrecid"]
    return recid


def get_ossr_pending_requests(zenodo_token):
    zen = ZenodoAPI(access_token=zenodo_token, sandbox=False)
    pending_requests = zen.get_community_pending_requests(escape_community, size=100)
    return pending_requests


def get_ossr_curation_opened_requests(ossr_pending_requests):

    gl = gitlab.Gitlab(GITLAB_URL)
    project = gl.projects.get(project_id)
    opened_mr = project.mergerequests.list(state="opened")

    requests_ids = [get_conceptrecid(req) for req in ossr_pending_requests]

    return [mr for mr in opened_mr if mr.source_branch in requests_ids]


def main():
    records = get_ossr_records()
    requests = get_ossr_pending_requests(zenodo_token)
    table = create_markdown_table(records, requests)

    output_dir = Path(__file__).parent.parent / "content" / "page"
    output_dir.mkdir(exist_ok=True)

    with open(output_dir / "ossr_records.md", "a") as f:
        f.write(
            f"There are currently {len(records)} curated records and {len(requests)} pending requests. Updated on {datetime.today().date()}.\n\n"
        )
        f.write(table)


if __name__ == "__main__":
    main()
