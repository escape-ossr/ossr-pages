[![pipeline status](https://gitlab.com/escape-ossr/ossr-pages/badges/master/pipeline.svg)](
https://gitlab.com/escape-ossr/ossr-pages/-/commits/master)

# Open-source Scientific Software and Service Repository (OSSR) portal

Visit the OSSR portal: [https://escape-ossr.gitlab.io/ossr-pages/](https://escape-ossr.gitlab.io/ossr-pages/)
  
--------- 
 

# Developers

These pages are run using [the Hugo framework](https://gohugo.io/).
Install Hugo on your system and clone this repository.

Then, you can run the following command to start a local server:

```bash
pip install git+https://gitlab.in2p3.fr/jschnabel/projectpagebuilder
python src/build_automatic_pages.py
hugo serve
```

## Tests

First build the site using `hugo`, then you can test that the pages are valid (images, urls...) using the following command:

```bash
docker run --rm -it \                                                                                                          ✱
  -v public:/src \
  klakegg/html-proofer:3.19.2 \
  --allow-hash-href --check-html --empty-alt-ignore --file-ignore ./index.html --url-ignore https://linkedin.com/in/escape-eu
```
